This script is not guaranteed to work with any apps, in fact, right now it barely works with any.
One day I just got tired of dealing with caches in my ~/.config, so I wrote a script to move most of them.
I know the code isn't the best, but I'm working on it still.
It should not be hard at all to add in more Electron-based apps. For most, you just have to copy one of the lines at the end of the script and change it to the name of whatever app you want to clear.
If you want me to try to include any apps, either not Electron-based or if you're just lazy, feel free to make an issue.
The apps it currently supports are available in a long line of commands at the end of the script.
If you're lazy, just use this command(POSIX): grep "clearConfigCache " cleanCaches.sh | sed 's/^.\{17\}//'
