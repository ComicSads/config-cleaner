#!/bin/sh
echo "Move every Cache found in config? y/n/c"
read -r Myn # note that this is the only variable where case is sensitive
if [ "$Myn" = c ]; then exit; fi 
if [ -z "$XDG_CONFIG_HOME" ]; then
        config="$HOME"/.config
else
        config="$XDG_CONFIG_HOME"
fi
if [ -z "$XDG_CACHE_HOME" ]; then
        cache="$HOME"/.cache
else
        cache="$XDG_CACHE_HOME"
fi

clearConfigCache() {
if [ -d "${config}"/"$*" ]; then # check if there's a cache directory in the config
	if [ "$Myn" = "n" ]; then
		echo "Do you want to move the Cache of $* to $cache? y/n/c"
		read -r yn
	else
		yn=$Myn
	fi
	case $yn in
		[Yy]* ) 
			rm -r "${config}"/"$*"/Cache 2> /dev/null # delete that cache directory
			mkdir -p "${cache}"/"$*" # create a cache directory for the app
			ln -sv "${cache}"/"$*" "${config}"/"$*"/Cache 2> /dev/null # symlink the app directory so it won't get recreated. Ignoring stderr so that when you re run this script it doesn't give you hundreds of "file exists" errors
		if [ -d "${config}"/"$*"/GPUCache ]; then # clear out GPU cache folders too
			rm -r "${config}"/"$*"/GPUCache 
			mkdir -p "${cache}"/"$*" 
			ln -sv "${cache}"/"$*" "${config}"/"$*"/GPUCache 2> /dev/null 
		fi
		specialCases "$*"
		;;
	[Nn]* )
		return
		;;
	[Cc]* )
		exit
		;;
	esac
fi
}

specialCases() {
case $* in
Franz) # Franz uses Chromium to run seperate instance of websites, which leads to many cache files in the config directory. The current solution isn't the best, as it moves ALL the Partition files in ~/.cache, rather than just the cache files.
		mkdir -p "$cache"/FranzApps
		mv "$config"/Franz/Partitions/* "$cache"/FranzApps 2> /dev/null # ignore stderr because if this program is run more than once, there will be problems because mv will think it's moving files to the same place
		ln -sv "$cache"/FranzApps/* "$config"/Franz/Partitions/ 2> /dev/null
	;;
browsh) # browsh uses firefox rofiles and stores it in ~/.config
		mkdir -p "$cache"/browshProfile
		mv "$config"/browsh/firefox_profile "$cache"/browshProfile 2> /dev/null 
		ln -sv "$cache"/browshProfile "$config"/browsh/firefox_profile 2> /dev/null
esac

}


clearConfigCache Keybase
clearConfigCache Franz
clearConfigCache nuclear
clearConfigCache Signal
clearConfigCache browsh
clearConfigCache discord
clearConfigCache Hyper
clearConfigCache Upterm
clearConfigCache Sunder
clearConfigCache Riot
clearConfigCache Joplin
clearConfigCache eDEX-UI
